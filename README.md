# Deploying Spark on Kubernetes

This project is a fork from [here](https://github.com/testdrivenio/spark-kubernetes) that deploys Apache Spark in a Kubernetes cluster.

Check out this [post](https://testdriven.io/deploying-spark-on-kubernetes) that shows how to build and deploy the original project.

Dashboard is available [here](https://spark.lincsproject.ca/)

Look at the [Spark on K8s](https://gitlab.com/calincs/infrastructure/spark-on-k8s) project for a more integrated way to work with Spark in the LINCS infrastructure.